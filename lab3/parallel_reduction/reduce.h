#ifndef _GPU_REDUCE_H_
#define _GPU_REDUCE_H_

///////////////////////////////////////////////////////////////////////////////////////////////
// INCLUDE: 
///////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////

#include <cuda_runtime.h>

#define DEFAULT_1D_BLOCK_SIZE 1024


///////////






///////////////////////////////////////////////////////////////////////////////////////////////
// REDUCE:
///////////////////////////////////////////////////////////////////////////////////////////////

template <typename T, unsigned int startSize>
void reduce(T *d_idata, T *d_odata);

///////////


///////////////////////////////////////////////////////////////////////////////////////////////
// TYPES:
///////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
struct SharedMemory {

	__forceinline__ __device__
	operator       T *() {
		extern __shared__ int __smem[];
		return (T *)__smem;
	}

	__forceinline__ __device__
	operator const T *() const {
		extern __shared__ int __smem[];
		return (T *)__smem;
	}
};

///////////






///////////////////////////////////////////////////////////////////////////////////////////////
// DEVICE:
// KERNEL (__global__):
//
// FAST REDUCTION:  
//    22GB per sec for double[1] type
//
// template meta-programing: 
//    generate function body for given { blockSize, nIsPow2 }. 
//    if statements are resolved at compile time.
///////////////////////////////////////////////////////////////////////////////////////////////

template <typename T, unsigned int blockSize, bool nIsPow2>
__global__ 
void kernel_reduce1(T *g_idata, T *g_odata, unsigned int n) {
	T *sdata = SharedMemory<T>();

	///////////////////////////////////
	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockSize*2 + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	T mySum = {0};

	///////////////////////////////////
	// we reduce multiple elements per thread.  The number is determined by the
	// number of active thread blocks (via gridDim).  More blocks will result
	// in a larger gridSize and therefore fewer elements per thread
	while (i < n) {
		mySum += g_idata[i];

		///////////////////////////////////
		// ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
		if (nIsPow2 || i + blockSize < n)
			mySum += g_idata[i+blockSize];

		i += gridSize;
	}

	///////////////////////////////////
	// each thread puts its local sum into shared memory
	sdata[tid] = mySum;
	__syncthreads();

	///////////////////////////////////
	// do reduction in shared mem
	...

	
	// here is an example how to do reduction along one warp
	// TODO: check if it can improve performance
#if (__CUDA_ARCH__ >= 300)
	if ( tid < 32 ) {
		///////////////////////////////////
		// Fetch final intermediate sum from 2nd warp
		if (blockSize >=  64) mySum += sdata[tid + 32];
        
		///////////////////////////////////
		// Reduce final warp using shuffle
		for (int offset = warpSize/2; offset > 0; offset /= 2) {
			mySum += __shfl_down(mySum, offset);
                        // NOTE: "__shfl_down()" function for cplx type does not exist
		}
	}
#else
	///////////////////////////////////
	// fully unroll reduction within a single warp
	// NOTE: There exists #pragma unroll, otherwise 
	...
#endif

	// write result for this block to global mem
	if (tid == 0) g_odata[blockIdx.x] = mySum;
}

///////////






///////////////////////////////////////////////////////////////////////////////////////////////
// AUXILIARY:
///////////////////////////////////////////////////////////////////////////////////////////////

__forceinline__ __host__
bool isPow2(unsigned int x) {
	return ((x&(x - 1)) == 0);
}

__forceinline__ __host__ 
unsigned int nextPow2(unsigned int x) {
	--x;
	x |= x >> 1;
	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x |= x >> 16;
	return ++x;
}

///////////






///////////////////////////////////////////////////////////////////////////////////////////////
// REDUCE INVOKER CORE VERSION:
//
// WARNING:
//    { threads, blocks } must be resolved at compile time. 
//    hint: use constexpr
//
// template meta-programing: 
//    generate function body for given { T - type }.
//    switch statements are resolved at compile time.
///////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
__forceinline__ __host__
void reduce_core(int size, int threads, int blocks, T *d_idata, T *d_odata) {
	dim3 dimBlock (threads, 1, 1);
	dim3 dimGrid  (blocks,  1, 1);

	///////////////////////////////////
	// when there is only one warp per block, we need to allocate two warps
	// worth of shared memory so that we don't index shared memory out of bounds
	int smemSize = (threads <= 32) ? 2 * threads * sizeof(T) : threads * sizeof(T);
	
	///////////////////////////////////
	// check threads:
	if (isPow2(size)) {
		switch (threads) {
			case 1024:
				kernel_reduce1<T, 1024, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 512:
				kernel_reduce1<T,  512, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 256:
				kernel_reduce1<T,  256, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 128:
				kernel_reduce1<T,  128, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 64:
				kernel_reduce1<T,   64, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 32:
				kernel_reduce1<T,   32, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 16:
				kernel_reduce1<T,   16, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case  8:
				kernel_reduce1<T,    8, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case  4:
				kernel_reduce1<T,    4, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case  2:
				kernel_reduce1<T,    2, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case  1:
				kernel_reduce1<T,    1, true><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;
		}
	}
	else {
		switch (threads) {
			case 1024:
				kernel_reduce1<T, 1024, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 512:
				kernel_reduce1<T,  512, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 256:
				kernel_reduce1<T,  256, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 128:
				kernel_reduce1<T,  128, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 64:
				kernel_reduce1<T,   64, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 32:
				kernel_reduce1<T,   32, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case 16:
				kernel_reduce1<T,   16, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case  8:
				kernel_reduce1<T,    8, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case  4:
				kernel_reduce1<T,    4, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case  2:
				kernel_reduce1<T,    2, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;

			case  1:
				kernel_reduce1<T,    1, false><<< dimGrid, dimBlock, smemSize >>>(d_idata, d_odata, size);
			break;
		}
	}
}

///////////






///////////////////////////////////////////////////////////////////////////////////////////////
// REDUCE INVOKER STANDARD VERSION:
//
// WARNING:
//    { threads, blocks } must be resolved at compile time. 
//
// template meta-programing: 
//    generate function body for given { blockSize, startSize }.
//    if statements are resolved at compile time.
///////////////////////////////////////////////////////////////////////////////////////////////

template <typename T, unsigned int startSize, int maxThreads> 
void reduce(T *d_idata, T *d_odata) {
	//const static int maxThreads  = DEFAULT_1D_BLOCK_SIZE;
	const static int startBlocks = (startSize + maxThreads - 1) / maxThreads;
	reduce_core <T> (startSize, maxThreads, startBlocks, d_idata, d_odata);

	/////////////////////////////////////
	int size    = startBlocks;
	int threads = maxThreads;
	int blocks  = (size + maxThreads - 1) / maxThreads;
	while (size > 1) {
		threads = (size < maxThreads * 2) ? nextPow2((size + 1) / 2) : maxThreads;
		blocks  = (size + (threads * 2 - 1)) / (threads * 2);
		
		cudaMemcpy(d_idata, d_odata, size * sizeof(T), cudaMemcpyDeviceToDevice);
		reduce_core <T> (size, threads, blocks, d_idata, d_odata);
		
		size = (size + (threads * 2 - 1)) / (threads * 2);
	}
}

/////////////////////////////////////
// TODO: Instantiate reduce


///////////



#endif 