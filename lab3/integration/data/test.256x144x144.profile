==4996== NVPROF is profiling process 4996, command: ./expected_val.exe
==4996== Profiling application: ./expected_val.exe
==4996== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.743933,153.585561,100,1.535855,1.520637,1.782455,"void kernel_RC_mult<int=256, int=144, int=144, int=1>(double const *, double2 const *, double2*)"
20.499769,122.299436,100,1.222994,1.199716,1.487294,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.846289,118.400843,100,1.184008,1.173829,1.218148,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.941968,101.073979,100,1.010739,0.992041,1.041064,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.683041,87.597459,100,0.875974,0.872172,0.907211,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.718508,10.252435,3,3.417478,0.001184,6.832386,"[CUDA memcpy HtoD]"
0.157837,0.941639,102,0.009231,0.003423,0.591218,"[CUDA memset]"
0.146718,0.875306,100,0.008753,0.008544,0.009567,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.095222,0.568085,400,0.001420,0.001343,0.001760,"[CUDA memcpy DtoH]"
0.072007,0.429589,100,0.004295,0.003808,0.004864,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.053273,0.317819,100,0.003178,0.003039,0.003424,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.041435,0.247195,100,0.002471,0.002431,0.002656,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==4996== API calls:
No API activities were profiled.
