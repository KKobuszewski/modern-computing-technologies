==4408== NVPROF is profiling process 4408, command: ./expected_val.exe
==4408== Profiling application: ./expected_val.exe
==4408== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.005807,45.961354,100,0.459613,0.455157,0.475189,"void kernel_RC_mult<int=128, int=112, int=112, int=1>(double const *, double2 const *, double2*)"
20.113718,36.969562,100,0.369695,0.363960,0.386326,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.489727,35.822649,100,0.358226,0.353784,0.374935,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
17.440390,32.055912,100,0.320559,0.311897,0.327864,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.671492,26.966602,100,0.269666,0.267705,0.281497,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.669002,3.067672,3,1.022557,0.000992,2.046864,"[CUDA memcpy HtoD]"
0.468803,0.861672,100,0.008616,0.008447,0.008960,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.308831,0.567639,400,0.001419,0.001312,0.001856,"[CUDA memcpy DtoH]"
0.287101,0.527700,102,0.005173,0.003392,0.177691,"[CUDA memset]"
0.224339,0.412341,100,0.004123,0.004032,0.004288,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.179456,0.329845,100,0.003298,0.003231,0.003488,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.141334,0.259776,100,0.002597,0.002560,0.002688,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==4408== API calls:
No API activities were profiled.
