==5375== NVPROF is profiling process 5375, command: ./expected_val.exe
==5375== Profiling application: ./expected_val.exe
==5375== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
26.130244,375.577142,100,3.755771,3.688492,4.257983,"void kernel_RC_mult<int=256, int=224, int=224, int=1>(double const *, double2 const *, double2*)"
20.559352,295.505181,100,2.955051,2.903742,3.238934,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.794663,284.514100,100,2.845141,2.820575,2.879998,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.740444,240.614977,100,2.406149,2.384969,2.434760,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.760448,212.155949,100,2.121559,2.102864,2.459336,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.714045,24.636430,3,8.212143,0.001312,16.477032,"[CUDA memcpy HtoD]"
0.121267,1.743002,102,0.017088,0.003168,1.416064,"[CUDA memset]"
0.060958,0.876171,100,0.008761,0.008480,0.009567,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.039951,0.574226,400,0.001435,0.001343,0.001760,"[CUDA memcpy DtoH]"
0.031255,0.449235,100,0.004492,0.004288,0.005056,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.029434,0.423063,100,0.004230,0.003712,0.008736,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.017940,0.257850,100,0.002578,0.002431,0.005344,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==5375== API calls:
No API activities were profiled.
