==4918== NVPROF is profiling process 4918, command: ./expected_val.exe
==4918== Profiling application: ./expected_val.exe
==4918== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.903311,122.332955,100,1.223329,1.198917,1.485405,"void kernel_RC_mult<int=256, int=128, int=128, int=1>(double const *, double2 const *, double2*)"
20.272526,95.740582,100,0.957405,0.949610,0.972714,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=1>(double const *, double2*, double*, unsigned int)"
19.800703,93.512314,100,0.935123,0.923498,0.951178,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.943029,80.016445,100,0.800164,0.786958,0.816109,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.687817,69.365806,100,0.693658,0.690512,0.706064,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.707663,8.064739,3,2.688246,0.001280,5.383874,"[CUDA memcpy HtoD]"
0.183308,0.865706,100,0.008657,0.008416,0.009440,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.167210,0.789678,102,0.007741,0.003168,0.460405,"[CUDA memset]"
0.121935,0.575861,400,0.001439,0.001343,0.007296,"[CUDA memcpy DtoH]"
0.090909,0.429335,100,0.004293,0.004000,0.005184,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.072194,0.340949,100,0.003409,0.003296,0.003616,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.049395,0.233275,100,0.002332,0.002303,0.002432,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==4918== API calls:
No API activities were profiled.
