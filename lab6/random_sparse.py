#
from __future__ import print_function, division

import numpy as np
import scipy.stats as stats
import scipy.sparse as sparse
import scipy.special as special
import matplotlib.pyplot as plt

N = 1024 # number of vertices
density = 0.02


#connectivity = scipy.sparse.random(N, N, density=0.1, random_state=2906, data_rvs=lambda x:1)

#np.random.seed((3,14159))
np.random.seed()

def sprandsym(n, density):
    #rvs = stats.norm().rvs
    rvs = lambda x:np.ones(x,dtype=np.float64)
    X = sparse.random(n, n, density=density, data_rvs=rvs)
    upper_X = sparse.triu(X) 
    result = upper_X + upper_X.T - 2.0*sparse.diags(X.diagonal())
    return result

M = sprandsym(N, density)
print(repr(M))
# <5000x5000 sparse matrix of type '<class 'numpy.float64'>'
#   with 249909 stored elements in Compressed Sparse Row format>

print()
print('designed density: ',density)
print('obtained density: ',M.nnz / float(N*N) )

# check that the matrix is symmetric. The difference should have no non-zero elements
assert (M - M.T).nnz == 0

M2 = M.dot(M)
M3 = M2.dot(M)

print(M3.toarray())
print(M3.diagonal())
print(M3.diagonal().sum() / 6.0)

print( special.binom( N, 3 ) )
print( special.binom( N, 3 ) * (density)**3 )