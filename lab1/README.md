**Laboratorium 1**

Praca z klastrem dwarf
======================

Logowanie na klaster
--------------------

Komenda do logowania

``` {frame="single" basicstyle="\scriptsize"}
ssh <login>@dwarf.if.pw.edu.pl
```

UWAGA: login i hasło zostaną podane przez prowadzącego

Przypomnienie: logowanie w trybie graficznym po dodaniu flagi -X / -Y

Systemy kontroli wersji
-----------------------

Wyniki wszystkich laboratoriów powinny znaleźć się w ogólnodostępnych
repozytoriach. Na klastrze dwarf jest dostępny program GIT.

Repozytorium z przykładami:
<https://gitlab.com/KKobuszewski/modern-computing-technologies>.

Należy przejrzeć tutorial:
<https://help.github.com/articles/fork-a-repo/> i prawidłowo stworzyć
kopię repozytorium z przykładami.

Konfiguracja konta
------------------

Należy wyedytować plik \~/.bashrc korzystając z przykładowego pliku
bashrc.template w repozytorium.

System kolejkowy - na przykładzie PBS/Torque
--------------------------------------------

#### Dostępne zasoby

W celu wylistowania dostępnych węzłów i ich zasobów należy użyć
polecenia

``` {frame="single" basicstyle="\scriptsize"}
[dteam050@node2072 ~]$ qnodes
node2072.grid4cern.if.pw.edu.pl
state = offline
np = 12
ntype = cluster
status = rectime=1538486042,varattr=,jobs=,state=free
...
gpus = 0
```

#### Sesja interaktywna

W celu utworzenia sesji inteaktywnej należy użyć skryptu session.sh

``` {frame="single" basicstyle="\scriptsize"}
session.sh <nodes> <ppn>
```

nodes - liczba używanych węzłów

ppn - liczba procesorów używanych na każdym węźle

#### Wrzucanie zadań do kolejki

``` {frame="single" basicstyle="\scriptsize"}
qsub job.sh
```

Testowanie oprogrmowania
------------------------

Wskazane jest testowanie każdej funkcji za pomocą testów jednostowych
(ang. unit tests). W tym laboratorium napisanie testów jednostkowych
jest zadaniem dodatkowym. Można do tego skorzystać z zewnętrznych
bibliotek, np. googletest.

Instrukcja instalacji biblioteki googletest

``` {frame="single" basicstyle="\scriptsize"}
git clone https://github.com/google/googletest.git
cd googletest && mkdir build && cd build
cmake -G"Unix Makefiles" ..
make
mkdir ~/libs/include && cp -r ../googletest/include/gtest ~/libs/include
mkdir ~/libs/lib     && cp lib/*.a ~/libs/lib/
```

Wstęp do testów można znaleźć tutaj:
<https://github.com/google/googletest/blob/master/googletest/docs/primer.md>.

Wprowadzenie do systemów z architekturą rozproszoną
===================================================

MPI
---

Jest to standard do przesyłania danych pomiędzy różnymi procesami.
Procesy niekoniecznie muszą wykonywać się na tym samym węźle klastra
obliczeniowego.

Ważne funkcje:

``` {frame="single" basicstyle="\scriptsize"}
MPI_Init
MPI_Barrier
MPI_Wtime
```

 \
Tzw. operacje kolektywne:

![image](collective_operations_mpi){width="100.00000%"}

Uruchamianie programu z MPI
---------------------------

Podstawowe komendy:

``` {frame="single" basicstyle="\scriptsize"}
mpirun -n <number of processes> <program> {[}OPTIONS{]}

mpirun -hostfile dwarfnodes.txt -n 2 list\_gpus\_mpi.x
```

Kompilacja biblioteki FFTW
--------------------------

Pobrać archiwum ze strony <http://www.fftw.org/download.html>

Skopiować plik archiwum na klaster DWARF.

Rozpakować

``` {frame="single" basicstyle="\scriptsize"}
tar -zxvf fftw-3.3.8.tar.gz

./configure CC=gcc --enable-mpi MPICC=mpicc --enable-shared MPILIBS=-lmpi
--enable-avx2

make install DESTDIR=/home/dteam050/libs

cp ../usr/local/lib/{*} ../lib/ \&\& cp ../usr/local/include/{*} ../include/
```

### Co robi biblioteka FFTW?

Jest to biblioteka do efektywnego liczenia dyskretnej transformaty
Fouriera za pomocą algorytmu Fast Fourier Transform.

Transformata ,,do przodu”

```math
\sum_{k=-N_{x}/2}^{N_{x}/2-1}\sum_{l=-N_{y}/2}^{N_{y}/2-1}\sum_{m=-N_{z}/2}^{N_{z}/2-1}e^{-2\pi i\left(\frac{k}{L_{x}}x_{k}+\frac{l}{L_{y}}y_{l}+\frac{m}{L_{z}}z_{m}\right)}f\left(x_{k},y_{l},z_{m}\right)
```

Transformata ,,do tyłu”

```math
\sum_{k=-N_{x}/2}^{N_{x}/2-1}\sum_{l=-N_{y}/2}^{N_{y}/2-1}\sum_{m=-N_{z}/2}^{N_{z}/2-1}e^{-2\pi i\left(\frac{k}{L_{x}}x_{k}+\frac{l}{L_{y}}y_{l}+\frac{m}{L_{z}}z_{m}\right)}\widehat{f}\left(k_{k}^{x},k_{l}^{y},k_{m}^{z}\right)
```

Liczenie pochodnych
-------------------

Należy wykorzystać wzór
$`\partial f/\partial x_{i}=\mathcal{F}^{-1}\left[ik_{i}\mathcal{F}\left[f\right]\right]`$
i dyskretną transformację Fouriera

```math
\mathcal{F}\left[f\right]=\int_{\mathbb{R}^{3}}e^{-i\boldsymbol{k}\cdot\boldsymbol{r}}f\left(\boldsymbol{r}\right)d^{3}r\approx\sum_{k=-N_{x}/2}^{N_{x}/2-1}\sum_{l=-N_{y}/2}^{N_{y}/2-1}\sum_{m=-N_{z}/2}^{N_{z}/2-1}e^{-2\pi i\left(\frac{k}{L_{x}}x_{k}+\frac{l}{L_{y}}y_{l}+\frac{m}{L_{z}}z_{m}\right)}f\left(x_{k},y_{l},z_{m}\right)\frac{L_{x}}{N_{x}}\frac{L_{y}}{N_{y}}\frac{L_{z}}{N_{z}}
```

$`\Delta k^{x}=\frac{2\pi}{L_{x}},\,\Delta x=\frac{L_{x}}{N_{x}}`$

$`x_{k}=k\cdot\Delta x,\,k_{k}^{x}=k\cdot\Delta k^{x}`$

```math
\mathcal{F}^{-1}\left[\widehat{f}\right]=\int_{\mathbb{R}^{3}}e^{i\boldsymbol{k}\cdot\boldsymbol{r}}\widehat{f}\left(\boldsymbol{k}\right)\frac{d^{3}k}{\left(2\pi\right)^{3}}=\frac{1}{\left(2\pi\right)^{3}}\sum_{k=-N_{x}/2}^{N_{x}/2-1}\sum_{l=-N_{y}/2}^{N_{y}/2-1}\sum_{m=-N_{z}/2}^{N_{z}/2-1}e^{2\pi i\left(\frac{k}{L_{x}}x_{k}+\frac{l}{L_{y}}y_{l}+\frac{m}{L_{z}}z_{m}\right)}\widehat{f}\left(k_{k}^{x},k_{l}^{y},k_{m}^{z}\right)\frac{2\pi}{L_{x}}\frac{2\pi}{L_{y}}\frac{2\pi}{L_{z}}
```

Czym się różnią wzory powyżej od wyniku dawanego przez FFTW?

Jaka jest różnica między dyskretną a ciągłą transformacją Fouriera? Jaki
jest ich związek z szeregiem Fouriera?

#### Zadanie 1.

Napisać klasę liczącą pochodne z wykorzystaniem bibliteki FFTW i MPI.
Zalecanie jest wcześniejsze przeanalizowanie przykładów z dokumentacji:
<http://fftw.org/doc/Multi_002ddimensional-MPI-DFTs-of-Real-Data.html#Multi_002ddimensional-MPI-DFTs-of-Real-Data>.

Gradient
--------

#### Zadanie 2.

Wykorzystując wcześniej stworzoną klasę liczącą pochodne oblicz gradient
pola skalarnego. 

```math
\nabla f=\left[\begin{array}{c}
\partial f/\partial x\\
\partial f/\partial y\\
\partial f/\partial z
\end{array}\right]
```

Jak przyspieszyć liczenie gradientu wykorzystując techniki równoległe?

Wskazówka: Stworzyć nowe komunikatory MPI.
