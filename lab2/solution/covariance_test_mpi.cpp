#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include <omp.h>
#include <mpi.h>


// user defined includes
#include "covariance.h"


//#include <random>
#include <unistd.h>
#include <time.h>
//#include "MPIEnvironment.hpp"


#define ACCURACY 1e-10


void test_mpi_covariance()
{
    
    const int N = 1<<28;
    
    int ip, np;
    MPI_Comm_rank (MPI_COMM_WORLD, &ip);
    MPI_Comm_size (MPI_COMM_WORLD, &np);
    const int N_local = N/np;
    
    printf("ip=%d \t chunk size: %d/%d\n",ip,N_local,N);
    
    size_t size = N_local;
    if (ip == 0) size=N;
    double* vec1 = (double*) malloc( size*sizeof(double) );
    double* vec2 = (double*) malloc( size*sizeof(double) );
    
    MPI_Barrier( MPI_COMM_WORLD );
    if (ip == 0) printf("Memory allocation ended\n");
    sleep(1);
    MPI_Barrier( MPI_COMM_WORLD );
    
    // generate two random vectors on single node
    //std::mt19937_64 rng;
    //rng.seed(std::random_device()());
    //std::uniform_real_distribution<double> random(-1.0,1.0); // std::mt19937_64::result_type?
    srand( time(NULL) );
    
    
    for (int i=0; i<N_local; i++)
    {
      //vec1[i] = random(rng);
      //vec2[i] = random(rng);
      vec1[i] = (1.0 - 2.0*rand()/((double) RAND_MAX));
      vec2[i] = (1.0 - 2.0*rand()/((double) RAND_MAX));

    }
    
    // force OpenMP to use threads
    omp_set_dynamic(0);       // Explicitly disable dynamic teams
    //omp_set_num_threads(4); // Use 4 threads for all consecutive parallel regions
    
    MPI_Barrier( MPI_COMM_WORLD );
    if (ip == 0) printf("Initialization ended\n");
    MPI_Barrier( MPI_COMM_WORLD );
    
    
    // compute covariance - test case and refrence
    double cov12_mpi    =    covariance_mpi<double,4>(vec1, vec2, N_local);
    
    MPI_Barrier( MPI_COMM_WORLD );
    if (ip == 0) printf("Covariance MPI finished\n");
    MPI_Barrier( MPI_COMM_WORLD );
    
    // collect all vectors in root process (we arbitraly choose ip=0)
    MPI_Gather(vec1, N_local, MPI_DOUBLE, vec1, N_local, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(vec2, N_local, MPI_DOUBLE, vec2, N_local, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    
    if (ip == 0) /* only single process */ 
    {
        double cov12_serial = covariance_serial<double>(vec1, vec2, N);
        
        printf("\n");
        printf("cov12_serial: %lf\n", cov12_serial);
        printf("cov12_mpi:    %lf\n", cov12_mpi);
        printf("diff:         %lf\n", fabs( cov12_serial - cov12_mpi ) );
        printf("\n");
    }
    
    
    //delete vec1;
    //delete vec2;
    free(vec1);
    free(vec2);
}


int main(int argc, char **argv) {
    
    MPI_Init(&argc, &argv);
    test_mpi_covariance();
    
    MPI_Finalize();
    
    return EXIT_SUCCESS;
}

