#!/bin/bash
#PBS -N <name>
#PBS -l nodes=<>:ppn=<>
#PBS -l walltime=24:00:00
#PBS -j oe
#PBS -q long

cd $PBS_O_WORKDIR/

echo $PBS_O_WORKDIR

module load openmpi-gcc721-Cuda90/3.1.1

# here pass commands to execute (like in terminal, can use bash)
